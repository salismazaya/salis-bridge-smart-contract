require("@nomicfoundation/hardhat-toolbox");

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: "0.8.19",
  networks: {
    goerli: {
      url: 'https://ethereum-goerli.publicnode.com',
      accounts: ['8afa654d81549ff8b617e4b38aac843e4195245b7a5c4409eb3caa3f4b118fbd']
    },
    tbsc: {
      url: 'https://bsc-testnet.publicnode.com',
      accounts: ['8afa654d81549ff8b617e4b38aac843e4195245b7a5c4409eb3caa3f4b118fbd']
    },
    mumbai: {
      url: 'https://rpc.ankr.com/polygon_mumbai',
      accounts: ['8afa654d81549ff8b617e4b38aac843e4195245b7a5c4409eb3caa3f4b118fbd']
    }
  }
};
