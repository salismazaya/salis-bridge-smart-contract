// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./IWormholeReceiver.sol";
import "./IWormholeRelayer.sol";
import "./AggregatorV3Interface.sol";

contract Contract is IWormholeReceiver {
    IWormholeRelayer private relayerAddress;
    AggregatorV3Interface private priceAggrerator;

    constructor(address _relayerAddress, address _priceAggrerator) {
        relayerAddress = IWormholeRelayer(_relayerAddress);
        priceAggrerator = AggregatorV3Interface(_priceAggrerator);
    }

    function quoteCrossChainGreeting(uint16 targetChain, uint256 gasLimit) public view returns (uint256 cost) {
        (cost,) = relayerAddress.quoteEVMDeliveryPrice(targetChain, 0, gasLimit);
    }

    function bridge(address targetAddress, address priceFeedAddress, uint16 wormholeChainId, address toContractAddress, uint256 gasLimit) public payable {
        uint256 cost = quoteCrossChainGreeting(targetChain, gasLimit);
        require(msg.value == cost, 'Cost not same');

        AggregatorV3Interface aggregator = AggregatorV3Interface(priceFeedAddress);
        (
            /* uint80 roundID */,
            int answer,
            /*uint startedAt*/,
            /*uint timeStamp*/,
            /*uint80 answeredInRound*/
        ) = aggregator.latestRoundData();

        (
            /* uint80 roundID */,
            int answer2,
            /*uint startedAt*/,
            /*uint timeStamp*/,
            /*uint80 answeredInRound*/
        ) = priceAggrerator.latestRoundData();

        uint256 amount = 

        relayerAddress.sendPayloadToEvm{value: cost}(
            wormholeChainId,
            toContractAddress,
            abi.encode(amount, targetAddress, msg.sender),
            0,
            gasLimit
        )
    }
}